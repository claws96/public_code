#!/usr/bin/perl

use CGI qw( :standard *table start_ul );
use CGI::Carp qw( fatalsToBrowser warningsToBrowser );
use DBI qw(:sql_types);
use Date::Calc qw( Add_Delta_Days Month_to_Text );
#use utf8;
#use strict;

$CGI::POST_MAX=10240;
$ROWS_PER_PAGE=30;

$dbi=DBI->connect("DBI:mysql:database=slashdot;host=localhost",
                  "slashdot", "sN0t", {RaiseError => 1});
$queryString = "select title,link,date_format(from_unixtime(date),'%d-%M-%Y %H:%i:%s' ) from slashdot.stories ";
$preparedQ{ date } = $dbi->prepare( qq{ $queryString where date >= unix_timestamp( str_to_date( ?, '%d-%M-%Y' ) ) and date <= unix_timestamp( str_to_date( ?, '%d-%M-%Y' ) ) + 86399 order by date desc } );
$preparedQ{ title } = $dbi->prepare( qq{ $queryString where title like ? order by date desc LIMIT ?,? } );
$preparedQ{ titleTotal } = $dbi->prepare( qq{ $queryString where title like ? } );

my @days = ( '01' .. '31' );
my @months = ( 'January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September',
               'October', 'November', 'December' );

my @boundsYears = $dbi->selectrow_array( "select date_format( from_unixtime( min( date ) ), '%Y' ), date_format( from_unixtime( max( date ) ), '%Y' ) from slashdot.stories" );
for( $i = $boundsYears[0]; $i <= $boundsYears[1]; $i++ ) { push( @years, $i ); }
my $hiddenDate = "";

$q = CGI->new; # Have to do this here so it's global.  Confusing as crout.

sub totalTitleResults() {
   $preparedQ{ titleTotal }->bind_param( 1, "%" . $q->param( 'title_search_string' ) . "%" );
   $preparedQ{ titleTotal }->execute;

   return( $preparedQ{ titleTotal }->rows );
}

sub drawTable( $ ) {
   local *exRef = shift;
   my( $title, $link, $date );

   if( $exRef->{Executed} ) { 
      $rv = $exRef->bind_columns( \$title, \$link, \$date );

      print $q->p( "Rows:" . $exRef->rows );
      print $q->start_table;

      while( $exRef->fetch ) {
	 print $q->Tr([ td([ $date, $q->a({href=>$link, target=>new}, $title ) ]) ]);
	 print( "\n" );
      }

      print $q->end_table;
   }
}

sub drawForm {

   if( $q->param( 'hidden_todays_date' ) ) {
      ( $hiddenTodaysYear, 
        $hiddenTodaysMonth, 
        $hiddenTodaysDay ) = split( /-/, $q->param( 'hidden_todays_date' ) );
   }
   else {
      $hiddenTodaysYear  = $q->param( 'todays_year' );
      $hiddenTodaysMonth = $q->param( 'todays_month' );
      $hiddenTodaysDay   = $q->param( 'todays_day' );
   }
   print $q->header( -type=>'text/html',
                     -charset=>'iso-8859-1' );

   print $q->start_html( -title=>"Nick's Slashdot Archive", 
                         -encoding=>'iso-8859-1');

   print( $q->start_form( -method=>'post' ) );
   
   print( $q->p( "Today's date:", 
	       $q->popup_menu( -name=>'todays_day',
			      -values=>\@days,
			      -override=>1,
			      -default=>$hiddenTodaysDay ),
	       $q->popup_menu( -name=>'todays_month',
			      -values=>\@months, 
			      -override=>1,
			      -default=>$hiddenTodaysMonth ),
	       $q->popup_menu( -name=>'todays_year',
			       -values=>\@years,
			       -override=>1,
			       -default=>$hiddenTodaysYear ),
	       $q->submit(     -name=>'todays_date',
			       -value=>'Search' ) ) );

   print( $q->p( "Starting date:", 
	       $q->popup_menu( -name=>'start_day',
			      -values=>\@days ),
	       $q->popup_menu( -name=>'start_month',
			      -values=>\@months ),
	       $q->popup_menu( -name=>'start_year',
			      -values=>\@years ),
	       "Ending date:",
	       $q->popup_menu( -name=>'end_day',
			      -values=>\@days ),
	       $q->popup_menu( -name=>'end_month',
			      -values=>\@months ),
	       $q->popup_menu( -name=>'end_year',
			      -values=>\@years ),
	       $q->submit(    -name=>'inclusive_date',
			      -value=>'Search' ) ) );

   print( $q->p( "Search in titles:",
	  $q->textfield( -name=>'title_search_string' ),
	  $q->submit(    -name=> 'title_search',
			 -value=> 'Search' ) ) );

   print( $q->p( $q->submit( -name=>'most_recent',
			     -value=>'Most Recent' ) ) );

   print( $q->p( $q->submit( -name=>'prev',
			     -value=>'<<' ),
	         $q->submit( -name=>'next',
			     -value=>'>>' ) ) );
   
   print( $q->p( $q->submit( -name=>'start_page',
                             -value=>"|<" ),
                 $q->submit( -name=>'prev_page',
			     -value=>'<' ),
	         $q->submit( -name=>'next_page',
			     -value=>'>' ),
	         $q->submit( -name=>'end_page',
			     -value=>'>|' ) ) );

   print( $q->hidden( -name=>'hidden_todays_date',
		      -default=>join( '-', $years[0], "January", 1 ) ) );
   
   $q->param( 'row_offset' ) > 0 ? printf( "<P>Showing stories %d through %d\n",$q->param( 'row_offset' ), $q->param( 'row_offset' ) + $ROWS_PER_PAGE )
                                 : printf( "<P>Showing stories %d through %d\n", 0, $ROWS_PER_PAGE );
   
   print( $q->hidden( -name=>'row_offset',
                      -default=>'0') );

   print $q->end_form;
   print $q->end_html;
}
   
   
if( $q->param( 'todays_date' ) eq "Search" or 
    $q->param( 'inclusive_date' ) eq "Search" or
    $q->param( 'most_recent' ) eq "Most Recent" or
    $q->param( 'prev' ) or
    $q->param( 'next' ) )  {

   my @arg = ( );  # Holds beginning, ending date for query.
   my $dateString; # Holds dd-MM-YYYY from SQL query.
   *handle = "";
   
   if( $q->param( 'most_recent' ) ) {
      @r =  $dbi->selectrow_array( qq{ select date_format( from_unixtime( max( date ) ), '%d-%M-%Y' ) from slashdot.stories } );  
      
      $dateString = $r[ 0 ];

      my @r1 = split( '-', $r[ 0 ] );
      $q->param( 'hidden_todays_date', join( '-', $r1[2], $r1[1], $r1[0] ) );

      @arg = ( $dateString, $dateString );

   }
   elsif( $q->param( 'prev' ) ) { # This has got to be the most confusing thing I've ever done.


      my @r1 = split( '-', $q->param( 'hidden_todays_date' ) );

      my @r2 = Add_Delta_Days( $r1[ 0 ], Date::Calc::Decode_Month( $r1[ 1 ] ), $r1[ 2 ], -1 );

      $dateString = join( '-', $r2[ 2 ], Date::Calc::Month_to_Text( $r2[ 1 ] ), $r2[ 0 ] );
      
      @arg = ( $dateString, $dateString );
      $q->param( 'hidden_todays_date', join( '-', $r2[ 0 ], 
                                       Date::Calc::Month_to_Text( $r2[ 1 ] ), $r2[ 2 ] ) );
   
   }
   elsif( $q->param( 'next' ) ) {
      
      my @r1 = split( '-', $q->param( 'hidden_todays_date' ) );

      my @r2 = Add_Delta_Days( $r1[ 0 ], Date::Calc::Decode_Month( $r1[ 1 ] ), $r1[ 2 ], 1 );

      $dateString = join( '-', $r2[ 2 ], Date::Calc::Month_to_Text( $r2[ 1 ] ), $r2[ 0 ] );
      
      @arg = ( $dateString, $dateString );
      $q->param( 'hidden_todays_date', join( '-', $r2[ 0 ], 
                                       Date::Calc::Month_to_Text( $r2[ 1 ] ), $r2[ 2 ] ) );
   
   }
   elsif( $q->param( 'todays_date' ) eq "Search" ) {
      my $dateString = join( '-', $q->param( "todays_day" ),
		                  $q->param( "todays_month" ),
				  $q->param( "todays_year" ) );
      
      @arg = ( $dateString, $dateString );
      $q->param( 'hidden_todays_date', join( '-', $q->param( "todays_year" ),
                                                  $q->param( "todays_month" ),
						  $q->param( "todays_day" ) ) );
   }
   elsif( $q->param( 'inclusive_date' ) eq "Search" ) {

      push( @arg, join( '-', $q->param( "start_day" ),
			     $q->param( "start_month" ),
			     $q->param( "start_year" ) ) );
      
      push( @arg, join( '-', $q->param( "end_day" ),
			     $q->param( "end_month" ),
			     $q->param( "end_year" ) ) );
   }
   $preparedQ{ date }->execute( @arg ) if @arg;
   $handle = \$preparedQ{ date };

}
elsif( ( $q->param( 'title_search' ) eq "Search" or
         $q->param( 'prev_page' ) or
	 $q->param( 'start_page' ) or
	 $q->param( 'end_page' ) or
         $q->param( 'next_page' ) ) && $q->param( 'title_search_string' ) ) {

   if( $q->param( 'prev_page' ) ) {
      if( $q->param( 'row_offset' ) - $ROWS_PER_PAGE > -1 ) {
         $q->param( 'row_offset', $q->param( 'row_offset' ) - $ROWS_PER_PAGE );
      }
      else {
         $q->param( 'row_offset', 0 );
      }
   }

   if( $q->param( 'next_page' ) ) {
      $q->param( 'row_offset', $q->param( 'row_offset' ) + $ROWS_PER_PAGE );
   }

   if( $q->param( 'start_page' ) ) {
      $q->param( 'row_offset', 0 );
   }
   
   if( $q->param( 'end_page' ) ) {
      $q->param( 'row_offset', totalTitleResults() - $ROWS_PER_PAGE );
   }
   
   $preparedQ{ title }->bind_param( 1, "%" . $q->param( 'title_search_string' ) . "%" );
   $preparedQ{ title }->bind_param( 2, $q->param( 'row_offset' ), { TYPE => SQL_INTEGER } );
   $preparedQ{ title }->bind_param( 3, $ROWS_PER_PAGE, { TYPE => SQL_INTEGER } );
   $preparedQ{ title }->execute();
   $handle = \$preparedQ{ title };

}
   
drawForm;
drawTable( $handle );
