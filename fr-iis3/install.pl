# Gets existing DACL and modifies Administrator rights
    use Win32::FileSecurity qw(MakeMask Get Set);
    use File::DosGlob 'glob';

$read_files = MakeMask( qw( READ ) ); # for files
$read_dirs = MakeMask( qw( READ GENERIC_READ GENERIC_EXECUTE ) ); # for directories

$change_files = MakeMask( qw( CHANGE ) ); # for files
$change_dirs = MakeMask( qw( CHANGE GENERIC_WRITE GENERIC_READ GENERIC_EXECUTE ) ); # for directories

$full_files = MakeMask( qw( FULL ) ); # for files
$full_dirs = MakeMask( qw( FULL  GENERIC_ALL ) ); # for directories

print "WARNING: Be sure you're running this script from the root of the distribution!!";
print "  Additionally, you should be Administrator to do this.\n";
print "If not, press CTRL-C to abort.\n\n";

print "Enter the name of the group that all Freereserves administrators belong to? [fr_users]: ";
$prompt = <STDIN>;
chop $prompt;

$prompt = "fr_users" if !$prompt;

if (!-e ".\\data") {
	print "Creading data dir...\n";
	system "mkdir data"; 
}
if (!-e ".\\logfiles") {
	print "Creading logfiles dir...\n";
	system "mkdir logfiles"; 
}

if (!-e ".\\readings") { 
	print "Creading readings dir...\n";
	system "mkdir readings"; 
}

push @FILES, glob('*\\*\\*');
push @FILES, glob('*\\*');
push @FILES, glob('*');
push @FILES, glob('.');

foreach( @FILES ) {
	s/\\/\\\\/g;
	s/\\$//;
	print "Processing $_\n";
	next unless -e;
	$hash{Administrator} = ( -d ) ? $full_dirs : $full_files ;
	$hash{"$prompt"} = ( -d ) ? $change_dirs : $change_files ;
	$hash{guests} = (-d ) ? $read_dirs : $read_files ;
	Set( $_, \%hash ) ;
    }

push @LOGFILES , glob('logfiles\\*');
push @LOGFILES , glob('logfiles');

foreach( @LOGFILES ) {
	s/\\/\\\\/g;
	s/\\$//;
	print "Processing $_\n";
	next unless -e;
	$hash{Administrator} = ( -d ) ? $full_dirs : $full_files ;
	$hash{"$prompt"} = ( -d ) ? $change_dirs : $change_files ;
	$hash{guests} = (-d ) ? $change_dirs : $change_files ;
	Set( $_, \%hash ) ;
    }
