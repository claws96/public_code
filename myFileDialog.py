import Tkinter, Tkconstants, tkFileDialog

class TkFileDialog(Tkinter.Frame):

  def __init__(self, root):

    Tkinter.Frame.__init__(self, root)

    # define options for opening or saving a file
    self.file_opt = options = {}
    options['defaultextension'] = '.txt'
    options['filetypes'] = [('all files', '.*'), ('text files', '.txt')]
    options['initialdir'] = ''
    options['initialfile'] = ''
    options['parent'] = root
    options['title'] = 'This is a title'
    options['multiple'] = True

    # defining options for opening a directory
    self.dir_opt = options = {}
    options['initialdir'] = 'C:\\'
    options['mustexist'] = False
    options['parent'] = root
    options['title'] = 'This is a title'

  def askopenfilenames(self):

    """Returns an opened file in read mode.
    This time the dialog just returns a filename and the file is opened by your own code.
    """

    # get filename
    fileNames = tkFileDialog.askopenfilenames(**self.file_opt)

    return( fileNames )

if __name__=='__main__':
  root = Tkinter.Tk()
  file = TkFileDialog(root).askopenfilenames()
  print( file )
  root.mainloop()

