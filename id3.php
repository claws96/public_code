#!/usr/bin/php
<?php
/*
   Takes mp3 files with a certain filename format, extracts the track number/title
   information, and puts that info in the id3 tag.

   format: id3.php path/to/mp3s/*

   - mp3 filenames should be in the format "<track> - <title>.mp3"
   - script extracts mp3 file title from filename & inserts it into
     id3 tag using utility.
*/

$totalSongs = $argc - 1;
for( $j = 1; $j < $argc; $j++ ) {
   
   $stuff = preg_split( '/\W\-\s/', basename( $argv[ $j ] ) );
   $trackNum = $stuff[0];
   $songTitle = substr( $stuff[1], 0, strlen( $stuff[1] ) - 4 );
   
   print( "Processing \"$songTitle\" {$trackNum}/{$totalSongs}\n" );
   system( "id3v2 -t \"{$songTitle}\" -T \"{$trackNum}/{$totalSongs}\" \"{$argv[ $j ]}\"\n" );
}
?>
