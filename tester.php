<?php
   /* WARNING: hack job.  Kinda clumsy.  Mis-named.  Stop laughing.  
      Was mostly a proof-of-concept, because storing tarballs and other binaries 
      in a MySQL database is dumb.
      
      Takes GET args:
        t:   table name inside "archive" database.
	a:   argument, either an insert id, or a filename.
	f:   function.  One of:
	     n  = get corresponding name from given insert id.
	     cf = check for existance of given filename (before we 
	          waste a bunch of bandwidth uploading it).
	     da = download all files in table.
	     dn = download file given name of file.
	     di = download file given insert id of file in table.
	     lf = list files in db.
	     mi = get max insert index of table.
	     up = upload a file to given table (file will be in
	          _POST[ 'upfile' ] ).
	          
		  A curl commandline for upload looks like this:
		  curl -F "upfile=@/root/slap_home.txt" "http://www.internalnet.zzz/tester.php?t=samples&f=up"
   */
   require_once( "includes.php" );
   if( ! isset( $_GET[ 't'] ) && ! isset( $_GET[ 'a' ] ) && ! isset( $_GET[ 'f' ] ) ) {
      if( strpos( $_SERVER[ 'HTTP_USER_AGENT' ], "libcurl" ) == 0 ) { print( "<PRE>" ); }
      print( "   
	 Takes GET args:
	 t:   table name inside \"archive\" database.
	 a:   argument, either an insert id, or a filename.
	 f:   function.  One of:
	       n  = get corresponding name from given insert id.
	       cf = check for existance of given filename (before we 
		     waste a bunch of bandwidth uploading it).
	       da = download all files in table.
	       dn = download file given name of file.
	       di = download file given insert id of file in table.
	       lf = list files in db.
	       mi = get max insert index of table.
	       up = upload a file to given table (file will be in
		     _POST[ 'upfile' ] ).
		     
		     A curl commandline for upload looks like this:
		     curl -F \"upfile=@/root/slap_home.txt\" \"http://www.internalnet.zzz/tester.php?t=samples&f=up\"
	 ");
      if( strpos( $_SERVER[ 'HTTP_USER_AGENT' ], "libcurl" ) == 0 ) { print( "</PRE>" ); }
      die();
   }
   $tables = array( );
   
   $dbRes = mysql_pconnect( "localhost", "archive", "wpass45" );

   /* SQL injection tests */
   $tq = mysql_query( "SHOW tables from archive" );
   $found = 0;
   while( $r = mysql_fetch_row( $tq ) ) { if( strcmp( $r[ 0 ], $_GET[ 't' ] ) == 0 ) { $found = 1; } }
   if( $found == 0 ) { die( "Specified table is not in database." ); }


   switch( $_GET[ 'f' ] ) {
      case "n": case "di":
	 $arg = sprintf( "%d", $_GET[ 'a' ] );
      break;

      case "cf": case "dn":
         $arg = addslashes( $_GET[ 'a' ] );
      break;
   
   }
   /**/
   
   switch( $_GET[ 'f' ] ) {
      
      case "n":
         $q = "SELECT index_num,name from archive.{$_GET[ 't' ]}_index where id='{$arg}'";
	 $r1 = mysql_fetch_assoc( mysql_query( $q ) );

	 print( strtolower( $r1[ 'name' ] ) );
	 exit;
      break;

      case "cf":
         $q = "SELECT id,name from archive.{$_GET[ 't' ]}_index where name like '%{$arg}%'";
	 $r1 = mysql_fetch_assoc( mysql_query( $q ) ); 

	 print isset( $r1 ) ? $r1[ 'id' ] : "0"; 
      break;

      case "dn":
         $q = "SELECT * FROM archive.{$_GET[ 't' ]} WHERE name='{$arg}'";
	 doDownload( mysql_query( $q ) );
      break;

      case "di":
         $q = "SELECT * FROM archive.{$_GET[ 't' ]} WHERE id='{$arg}'";
	 doDownload( mysql_query( $q ) );
      break;

      case "lf":
         $q = "SELECT id,name,mimetype,size,date from archive.{$_GET[ 't' ]}";
	 $q1 = mysql_query( $q );
	 if( ! preg_match( "/curl/", $_SERVER['HTTP_USER_AGENT' ] ) ) { 
	    print( "<PRE>\n" );
	 }
	 while( $r1 = mysql_fetch_row( $q1 ) ) {
	    for( $j = 0; $j < 4; $j++ ) {
	       $d = $r1[ $j ];
	       print( "{$d}\t\t\t" );
	    }
	    print( "\n" );
	 }
	 if( ! preg_match( "/curl/", $_SERVER['HTTP_USER_AGENT' ] ) ) { 
	    print( "</PRE>\n" );
	 }
      break;
      
      case "mi":
	 print( getMaxRows( ) );
      break;

      case "up":
         if( is_uploaded_file( $_FILES[ 'upfile' ][ 'tmp_name' ] ) ) { 
	    print( doUpload( ) ); 
	 } else { 
	    print_r( $_GET );
	    print_r( $_POST );
	    print_r( $_FILES );
	    die( "is not a valid upload." ); 
         } 
      break;
   
   }

   function getMaxRows( ) {
      $q="SELECT MAX(id) from archive.{$_GET[ 't' ]}";  
      return( array_pop( mysql_fetch_row( mysql_query( $q ) ) ) );
   }

   function doDownload( $queryHandle ) {

      $row = mysql_fetch_assoc( $queryHandle );

      $type = $row[ 'mimetype' ];
      $name = $row[ 'name' ];
      $size = $row[ 'size' ];
      $stuff = $row[ 'file_content' ];
      
      header('Content-type: ' . $type);
      header('Content-Disposition: attachment; filename="' . $name . '"');
      header('Content-length: ' . $size );
      print( $stuff );
   }

   function doUpload( ) {

      $fileName = $_FILES[ 'upfile' ][ 'name' ];
      $size = filesize( $_FILES[ 'upfile' ][ 'tmp_name' ] );
      $type = $_FILES[ 'upfile' ][ 'type' ];
      $date = getUnixTime();
      $content = addslashes( file_get_contents( $_FILES[ 'upfile' ][ 'tmp_name' ] ) );
      
      $q = mysql_query( "INSERT INTO archive.{$_GET[ 't' ]} SET id='',name='{$fileName}',file_content='{$content}',mimetype='{$type}',size='{$size}',date='{$date}'" ) or die( "Can't insert --" . mysql_error() );
      $newIndex = mysql_insert_id( );
      $q2 = mysql_query( "INSERT INTO archive.{$_GET[ 't' ]}_index SET id='',name='{$fileName}',index_num='{$newIndex}'" );
      
      if( $newIndex > 0 && $q2 ) { return( $newIndex ); }
   }

?>
