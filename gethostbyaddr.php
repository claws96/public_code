#!/usr/bin/php -r

# So I don't have to put the host utils package on the machine.

   if ( ! $argv[1] ) {
      exit(1);
   }

   echo gethostbyaddr( $argv[1] );
