//Header file for CS220 project #6
//Written by Paul McCubbins
#include <stdio.h>

// Changes to the datatype of the array (tree) should echo throughout
// the project.  I'm trying to stay away from templates because of 
// gcc's crankiness.
typedef char btree_data_type;

class btree_array {
   
   public:
      btree_array( int init_size );
      
      // These functions return the array index of the corresponding
      // tree element. "index" is the array index of the parent of
      // the element to find.
      int left( int parent_index );
      int right( int parent_index );
      btree_array parent( int parent_index );

      // For debugging. Just steps through the tree
      // array and prints each element.
      void see();		
      
      // "fill" is used to fill the tree (surprise).  
      // "contents" should be an array full of data elements
      // of whatever type that is specified in the typedef
      // at the beginning of this file.
      void fill( btree_data_type *contents );
      
      // Recurses through, and displays, the tree in in-order style.
      // "index" is the index of the parent node at which to start
      // the recursion.
      void inorder( int cursor_index );
      
      // Recurses through, and displays, the tree in pre-order style.
      // "index" is the index of the parent node at which to start
      // the recursion.
      void preorder( int cursor_index );
      
   private:
      btree_data_type *array;
      int size;			// Size of the array.
};
