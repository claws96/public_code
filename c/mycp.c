/* For CS306 Lab 3, due 5/1/03:
 * - Added fcntl for write lock ( lines 39 - 45 )
 */

#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>

void printUsage( char **arguments );
void errorMsg( char *prog_name, char *arguments ); 

const int BUFSIZE = 1024;

int main( int argc, char **argv ) {
   
   int outfileDescriptor = 1;	/* Default to stdout */
   int infileDescriptor = 0;	/* Default to stdin  */
   int charBuffer[ BUFSIZE ];
   int bytesRead = 0;
   struct flock f_lock;

   switch( argc ) {
      case 2:			/* Output to stdout */
	 if( ( infileDescriptor = open( argv[1], O_RDONLY ) )  < 0 )
	    errorMsg( argv[0], argv[1] );
      break;

      case 3:			/* Copy file1 -> file2 */
	 if( ( infileDescriptor = open( argv[1], O_RDONLY ) )  < 0 )
	    errorMsg( argv[0], argv[1] );
	 
	 if( ( outfileDescriptor = open( argv[2], O_CREAT | O_TRUNC | O_WRONLY, 0666 ) )  < 0 )
	    errorMsg( argv[0], argv[2] );

	 f_lock.l_type = F_WRLCK;	/* Set options for obtaining */
	 f_lock.l_whence = SEEK_SET;    /* write lock on whole file  */
	 f_lock.l_start = 0;
	 f_lock.l_len = 0;

	 /* F_SETLKW means fcntl will wait for lock to happen: */
	 fcntl( outfileDescriptor, F_SETLKW, &f_lock );

      break;

      default:			
	 printUsage( argv );
	 exit( 3 );
      break;
   }	 
   
   while( ( bytesRead = read( infileDescriptor, charBuffer, BUFSIZE ) ) )
      if ( bytesRead == -1 ) 
	 errorMsg( argv[0], argv[1] );
      else
	 write( outfileDescriptor, charBuffer, bytesRead );

   if( ( close( infileDescriptor ) < 0 ) )
      errorMsg( argv[0], argv[1] );
   
   if( ( close( outfileDescriptor ) < 0 ) )
      errorMsg( argv[0], argv[2] );

exit( 0 );
}

void errorMsg( char *prog_name, char *arguments ) {
   char concat_msg[256];

   strncpy( concat_msg, prog_name, 256 );
   strncat( concat_msg, ": ", 256 );
   strncat( concat_msg, arguments, 256 );

   perror( concat_msg );

exit( errno );
}

void printUsage( char **arguments ) {
   fprintf( stderr, "%s v1.1, Written by Paul McCubbins\n\n", arguments[0] );
   fprintf( stderr, "Usage: %s source [dest]\n", arguments[0] );
   fprintf( stderr, "Copy source to dest.  If dest is omitted, copy to standard output.\n\n" );
   fprintf( stderr, "Returns 0 on success, <errno> upon error.\n" );
}

