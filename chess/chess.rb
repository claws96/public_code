#!/usr/bin/env ruby

class Chessercise

  def initialize(pn, cp)
    @piece_name = pn.downcase if pn
    @current_position = cp.downcase if cp

    unless valid_input_params?
      puts invalid_params_message
	  exit 1
    end
   
  end
  
  def do_moves
    method_name = @piece_name + '_valid_positions'
    positions = public_send(method_name) if respond_to? method_name
    positions
  end
  
  def show_moves
    puts pretty_format(do_moves)
  end

  def return_moves
    pretty_format(do_moves)
  end 
  
  def rook_valid_positions
	valid_positions = []
	original_position = @current_position
	
	# up
	while 1
	  @current_position = move_up
	  break if original_position == @current_position or @current_position == false
	  valid_positions.push @current_position if current_is_valid_position?
	end

	@current_position = original_position
	#down
	while 1
	  @current_position = move_down
	  break if original_position == @current_position or @current_position == false
	  valid_positions.push @current_position if current_is_valid_position?
	end
	
	@current_position = original_position
	#right
	while 1
	  @current_position = move_right
	  break if original_position == @current_position or @current_position == false
	  valid_positions.push @current_position if current_is_valid_position?
	end

	@current_position = original_position
	#left
	while 1
	  @current_position = move_left
	  break if original_position == @current_position or @current_position == false
	  valid_positions.push @current_position if current_is_valid_position?
	end

	valid_positions.uniq
  end

  def queen_valid_positions
	valid_positions = []
	original_position = @current_position
	
	# up
	while 1
	  @current_position = move_up
	  break if original_position == @current_position or @current_position == false
	  valid_positions.push @current_position if current_is_valid_position?
	end

	@current_position = original_position
	#down
	while 1
	  @current_position = move_down
	  break if original_position == @current_position or @current_position == false
	  valid_positions.push @current_position if current_is_valid_position?
	end
	
	@current_position = original_position
	#right
	while 1
	  @current_position = move_right
	  break if original_position == @current_position or @current_position == false
	  valid_positions.push @current_position if current_is_valid_position?
	end

	@current_position = original_position
	#left
	while 1
	  @current_position = move_left
	  break if original_position == @current_position or @current_position == false
	  valid_positions.push @current_position if current_is_valid_position?
	end

	@current_position = original_position
	#up and right
	while 1
	  @current_position = move_up
	  @current_position = move_right
	  break if original_position == @current_position or @current_position == false
	  valid_positions.push @current_position if current_is_valid_position?
	end
	
	@current_position = original_position
	#up and left 
	while 1
	  @current_position = move_up
	  @current_position = move_left
	  break if original_position == @current_position or @current_position == false
	  valid_positions.push @current_position if current_is_valid_position?
	end
	
	@current_position = original_position
	#down and right
	while 1
	  @current_position = move_down
	  @current_position = move_right
	  break if original_position == @current_position or @current_position == false
	  valid_positions.push @current_position if current_is_valid_position?
	end
	
	@current_position = original_position
	#down and left
	while 1
	  @current_position = move_up
	  @current_position = move_left
	  break if original_position == @current_position or @current_position == false
	  valid_positions.push @current_position if current_is_valid_position?
	end
	
	valid_positions.uniq
  end

  def knight_valid_positions

	valid_positions = []

	# up and left
	original_position = @current_position

	@current_position = move_up
	@current_position = move_up
	@current_position = move_left
	
	valid_positions.push @current_position if current_is_valid_position?

	# up and right
	@current_position = original_position

	@current_position = move_up
	@current_position = move_up
	@current_position = move_right

	valid_positions.push @current_position if current_is_valid_position?
	
	# down and left
	@current_position = original_position

	@current_position = move_down
	@current_position = move_down
	@current_position = move_left

	valid_positions.push @current_position if current_is_valid_position?

	# down and right
	@current_position = original_position

	@current_position = move_down
	@current_position = move_down
	@current_position = move_right

	valid_positions.push @current_position if current_is_valid_position?

	valid_positions.uniq
  end

  private 
	def move_left
	  return false unless @@columns.index(current_column)
	  @@columns.index(current_column) > 0 ? @@columns[@@columns.index(current_column) - 1] + current_row.to_s : false 
	end

	def move_right
	  return false unless @@columns.index(current_column)
	  @@columns.index(current_column) < 7 ? @@columns[@@columns.index(current_column) + 1] + current_row.to_s : false
	end

	def move_up
	  return false unless current_is_valid_position?
	  current_row_i = current_row.to_i
	  current_row_i < 8 ? current_column.to_s + (current_row_i + 1).to_s : false 
	end

	def move_down
	  return false unless current_is_valid_position?
	  current_row_i = current_row.to_i
	  current_row_i >= 1 ? current_column.to_s + (current_row_i - 1).to_s : false 
	end

	def pretty_format(data)
	  return_string = ''

	  data.each do |d|
	    return_string += d
	    return_string += ', ' unless data.last == d
	  end

	  return_string
	end

	def current_row
	  @current_position[1]
	end

	def current_column
	  return false unless current_is_valid_position? or @current_position
	  @current_position[0]
	end

	def piece_name_valid?
	  return false if @piece_name.nil?
	  self.respond_to? @piece_name + '_valid_positions'
	end

	def current_is_valid_position?
	  @current_position =~ /[abcdefgh][12345678]/
	end

	def valid_input_params?
	  piece_name_valid? and current_is_valid_position?
	end

	def invalid_params_message
	  puts "Input parameters should be (queen|rook|knight) piece_position"
	end

end
