require_relative '../chess.rb'

describe Chessercise do
  it 'should output a correct set of queen moves' do
	c = Chessercise.new('queen', 'e5')
    expect{ c.show_moves }.to output("e6, e7, e8, e4, e3, e2, e1, f5, g5, h5, d5, c5, b5, a5, f6, g7, h8, d6, c7, b8, f4, g3, h2\n").to_stdout
  end
  
  it 'should output a correct set of rook moves' do
	c = Chessercise.new('rook', 'e5')
    expect{ c.show_moves }.to output("e6, e7, e8, e4, e3, e2, e1, f5, g5, h5, d5, c5, b5, a5\n").to_stdout
  end

  it 'should output a correct set of knight moves' do
	c = Chessercise.new('knight', 'e5')
    expect{ c.show_moves }.to output("d7, f7, d3, f3\n").to_stdout
  end

  it 'should output a correct set of queen moves without incorrect moves' do
	c = Chessercise.new('queen', 'a1')
    expect{ c.show_moves }.to output("a2, a3, a4, a5, a6, a7, a8, b1, c1, d1, e1, f1, g1, h1, b2, c3, d4, e5, f6, g7, h8\n").to_stdout
  end
  
  it 'should output a correct set of rook moves without incorrect moves' do
	c = Chessercise.new('rook', 'a1')
    expect{ c.show_moves }.to output("a2, a3, a4, a5, a6, a7, a8, b1, c1, d1, e1, f1, g1, h1\n").to_stdout
  end
  
  it 'should output a correct set of knight moves without incorrect moves' do
	c = Chessercise.new('knight', 'a1')
    expect{ c.show_moves }.to output("b3\n").to_stdout
  end
  

end
