#!/usr/local/bin/php
<?php

date_default_timezone_set("UTC");

$fh = fopen( "testparse.txt", "r" ); 
$fi = fopen( "testparsed.txt", "w" );
$parseString = "F: %s %s %1s %1s %1s { %s } # %2s-%2s-%4s ; %s\n";
$nowDate = strtotime( date( "d-m-Y" ) );

while( $line = fgets( $fh ) ) {

   $parsedDate = "";
   
   # Remove extraneous newlines at end:
   $line = preg_replace( array( "/\r\n$/", "/\n$/" ), "", $line );
   
   if( substr( $line, 1, 1 ) != "#" )
      $inputElements = sscanf( $line, $parseString );
    else
      $inputElements = sscanf( $line, "#" . $parseString );

    if( strlen( $inputElements[ 7 ] . $inputElements[ 6 ] . $inputElements[ 8 ] ) > 0 )  
      $parsedDate = mktime( 0, 0, 0, $inputElements[ 7 ], $inputElements[ 6 ], $inputElements[ 8 ] );
  
    if( !isset( $inputElements[ 0 ] ) ) {
       #Line is a comment, or ??
       fwrite( $fi, $line . "\n" );
       continue;
    }
   
    if( !$parsedDate )
       continue;

    if( $parsedDate > $nowDate )
      fprintf( $fi, $parseString, $inputElements[ 0 ],
                                  $inputElements[ 1 ],
                                  $inputElements[ 2 ],
                                  $inputElements[ 3 ],
                                  $inputElements[ 4 ],
                                  $inputElements[ 5 ],
                                  $inputElements[ 6 ],
                                  $inputElements[ 7 ],
                                  $inputElements[ 8 ],
                                  $inputElements[ 9 ] );
    
    elseif( $parsedDate <= $nowDate )
      # Consider a date equal to today, to be expired.
      
      fprintf( $fi, "#" . $parseString, $inputElements[ 0 ],
                                        $inputElements[ 1 ],
                                        $inputElements[ 2 ],
                                        $inputElements[ 3 ],
                                        $inputElements[ 4 ],
                                        $inputElements[ 5 ],
                                        $inputElements[ 6 ],
                                        $inputElements[ 7 ],
                                        $inputElements[ 8 ],
                                        $inputElements[ 9 ] );



}

?>
